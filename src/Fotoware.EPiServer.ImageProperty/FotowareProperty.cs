﻿using System.Globalization;
using EPiServer.Web.WebControls;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Setting the custom attributes of fotoware property
    /// </summary>
    public class FotowareProperty : Property
    {
        public const string UIHint = "Fotoware";

        public string AlternateText
        {
            get { return RenderSettings.GetAttribute("AlternateText"); }
            set { RenderSettings.SetAttribute("AlternateText", value); }
        }


        public string AltMetaCode
        {
            get { return RenderSettings.GetAttribute("AltMetaCode"); }
            set { RenderSettings.SetAttribute("AltMetaCode", value); }
        }

        public string ToolTipMetaCode
        {
            get { return RenderSettings.GetAttribute("ToolTipMetaCode"); }
            set { RenderSettings.SetAttribute("ToolTipMetaCode", value); }
        }
    }
}