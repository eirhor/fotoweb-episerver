﻿using System;
using System.Web.UI.WebControls;
using EPiServer.Events.Clients;
using EPiServer.PlugIn;

namespace Fotoware.EPiServer.ImageProperty
{
    [GuiPlugIn(Area = PlugInArea.None)]
    public class EpiserverSettings
    {
        internal static Guid BroadcastSettingsChangedEventId = new Guid("7B69F3B8-6001-47E0-855B-14FC07A97FFE");
        private static EpiserverSettings instance;
        
        [PlugInProperty(Description = "FotoWeb server URL", AdminControl = typeof(TextBox), AdminControlValue = "Text")]
        public string FotowareImageServiceAddress { get; set; }

        [PlugInProperty(Description = "FotoWeb encryption secret", AdminControl = typeof(TextBox), AdminControlValue = "Text")]
        public string SecretKey { get; set; }

        private EpiserverSettings()
        {
            PlugInSettings.SettingsChanged += BroadcastToAllServers;

            Event broadcastSettingsChangedEvent = Event.Get(BroadcastSettingsChangedEventId);
            broadcastSettingsChangedEvent.Raised += (sender, e) => { instance = null; };
        }

        public void BroadcastToAllServers(object sender, EventArgs e)
        {
            Event settingsChangedEvent = Event.Get(BroadcastSettingsChangedEventId);
            settingsChangedEvent.Raise(BroadcastSettingsChangedEventId, null);
        }

        public static EpiserverSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EpiserverSettings();
                    PlugInSettings.AutoPopulate(instance);
                }
                return instance;
            }
        }
    }
}