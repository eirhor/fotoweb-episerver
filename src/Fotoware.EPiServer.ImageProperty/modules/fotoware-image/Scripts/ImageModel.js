﻿
define("fotoware-image/ImageModel", [
// dojo
        "dojo/_base/array"
],

function (array) {


     function Metas() {

        this.caption;
        this.tooltip;
        this.publication;
        this.metaData = [];

    };


    function ImageModel(image) {
        self = this;

        self.storaeId = 0;
        self.doubleSizeUrl = null;
        self.url = null;
        self.thumbUrl = null;
        self.width = null;
        self.height = null;
        self.metas = new Metas();

        var self = this;
        if (image != null) {
            self.localId = image.StorageId;
            self.doubleSizeUrl = image.DoubleSizeUrl;
            self.url = image.Url;
            self.thumbUrl = image.Url;
            self.width = image.Width;
            self.height = image.Height;
            self.metas = image.Metas;
        };
    }

    return ImageModel;
});