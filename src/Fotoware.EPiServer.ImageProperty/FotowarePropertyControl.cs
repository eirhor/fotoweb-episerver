﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EPiServer.Core;
using EPiServer.Editor;
using EPiServer.Framework.DataAnnotations;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.PropertyControls;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Implementaion of property Episerver property contorl
    /// </summary>
    [TemplateDescriptor(TagString = FotowareProperty.UIHint)]
    public class FotowarePropertyControl : PropertyUrlControl, IRenderTemplate<FotowareImage>
    {

        /// <summary>
        /// Creates set of the default controls buiding the final contorl
        /// </summary>
        public override void CreateDefaultControls()
        {
            //Setup image control
            var image = new Image
            {
                ToolTip = this.AttributeSourceControl.ToolTip
            };

            //Setup Image properties
            if (PropertyData != null && PropertyData.Value != null)
            {
                var valueImage = (FotowareImage) PropertyData.Value;
                SetImageProperties(valueImage, image);
            }
          
            Controls.Add(image);
        }

        /// <summary>
        /// Sets the image properties.
        /// </summary>
        /// <param name="valueImage">The value image.</param>
        /// <param name="image">The image.</param>
        private void SetImageProperties(FotowareImage valueImage, Image image)
        {
            if (!PageEditing.PageIsInEditMode)
            {
                if (!ContentReference.IsNullOrEmpty(valueImage.ContentLink))
                {
                    var imageStorage = ServiceLocator.Current.GetInstance<IImageStorage>();
                    string url = imageStorage.GetUrl(valueImage.ContentLink);
                    image.ImageUrl = string.IsNullOrEmpty(url) ? valueImage.Url : url;
                }
                else
                {
                    image.ImageUrl = valueImage.Url;
                }
            }
            else
            {
                image.ImageUrl = valueImage.Url;
            }

            image.Width = new Unit(valueImage.Width);
            image.Height = new Unit(valueImage.Height);
           
            SetAltText(valueImage, image);
            SetToolTipText(valueImage, image);
        }

        /// <summary>
        /// Sets the tool tip text. ToolTipMetaCode have priority then the custom user tooltip value is taken. If non given the fotoware default meta tooltip is taken.
        /// </summary>
        /// <param name="valueImage">The value image.</param>
        /// <param name="image">The image.</param>
        private void SetToolTipText(FotowareImage valueImage, Image image)
        {

            if (RenderSettings.ContainsKey("ToolTipMetaCode"))
            {
                var metaCode = (string)RenderSettings["ToolTipMetaCode"];
                if (string.IsNullOrEmpty(metaCode))
                {
                    RenderDefaultTooltip(valueImage, image);
                }
                else
                {
                    string metaValue;
                    if (valueImage.Metas.MetaData.TryGetValue(metaCode, out metaValue))
                    {
                        image.ToolTip = metaValue;
                    }
                    else
                    {
                        RenderDefaultTooltip(valueImage, image);
                    }
                }
            }
            else
            {
                RenderDefaultTooltip(valueImage, image);
            }
        }

        /// <summary>
        /// Renders the default tooltip taken from property attribute or fotoware Image meta data
        /// </summary>
        /// <param name="valueImage">The value image.</param>
        /// <param name="image">The image.</param>
        private void RenderDefaultTooltip(FotowareImage valueImage, Image image)
        {
            image.ToolTip = string.IsNullOrEmpty(this.AttributeSourceControl.ToolTip) ? valueImage.Metas.Tooltip : this.AttributeSourceControl.ToolTip;
        }

        /// <summary>
        /// Sets the alt text. AltMetaCode have priority then the custom user Alt text value is taken. If non given the fotoware default Metas.Caption is taken.
        /// </summary>
        /// <param name="valueImage">The value image.</param>
        /// <param name="image">The image.</param>
        private void SetAltText(FotowareImage valueImage, Image image)
        {
            if (RenderSettings.ContainsKey("AltMetaCode"))
            {
                var metaCode = (string) RenderSettings["AltMetaCode"];
                if (string.IsNullOrEmpty(metaCode))
                {
                    RenderDefaultMetaAlt(valueImage, image);
                }
                else
                {
                    string metaValue;
                    if (valueImage.Metas.MetaData.TryGetValue(metaCode, out metaValue))
                    {
                        image.AlternateText = metaValue;
                    }
                    else
                    {
                        RenderDefaultMetaAlt(valueImage, image);
                    }
                }
            }
            else
            {
                RenderDefaultMetaAlt(valueImage, image);
            }
        }

        /// <summary>
        /// Renders the default meta alt. taken form atrribue or Image meta alt
        /// </summary>
        /// <param name="valueImage">The value image.</param>
        /// <param name="image">The image.</param>
        private void RenderDefaultMetaAlt(FotowareImage valueImage, Image image)
        {
            image.AlternateText = RenderSettings.ContainsKey("AlternateText")
                ? (string) RenderSettings["AlternateText"]
                : valueImage.Metas.Caption;
        }

        private class ImageContainerControl : WebControl
        {
            protected ImageContainerControl()
            {
            }

            public ImageContainerControl(HtmlTextWriterTag tag)
                : base(tag)
            {
            }
        }
    }
}